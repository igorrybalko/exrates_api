<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\CacheApiHelper;

class ApiController extends Controller
{
    public function forex()
    {
        $cacheApiHelper = new CacheApiHelper();
        $result = $cacheApiHelper->getData('forex.json', 'http://apilayer.net/api/live?access_key=a8dbeee8c4f6a55a44dd50e944cdd832&currencies=EUR,GBP,CHF,JPY,PLN,CNY,RUB&source=USD&format=1');
        return $result;
    }
    public function bankExchange()
    {
        $cacheApiHelper = new CacheApiHelper();
        $result = $cacheApiHelper->getData('bankexchange.json', 'http://resources.finance.ua/ru/public/currency-cash.json');
        return $result;
    }

    public function nbu()
    {
        $cacheApiHelper = new CacheApiHelper();
        $result = $cacheApiHelper->getData('nbu.json', 'http://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json');
        return $result;
    }

    public function crypto()
    {
        $cacheApiHelper = new CacheApiHelper();
        $result = $cacheApiHelper->getData('crypto.json', 'https://api.coinmarketcap.com/v1/ticker/', 'crypto');
        return $result;
    }

    public function news()
    {
        //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $cacheApiHelper = new CacheApiHelper();
        $result = $cacheApiHelper->getData('news.json', 'http://k.img.com.ua/rss/ru/financial.xml', 'xml');

        return $result;
    }
}
