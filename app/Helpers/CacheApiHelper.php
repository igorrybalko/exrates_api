<?php

namespace App\Helpers;

class CacheApiHelper{

    private $expires = 3600; //Время жизни кэша в секундах

    private function writeCache($cacheFileName, $apiUrl, $type)
    {
        switch ($type){
            case 'crypto':
                $apiData = str_replace('24h_volume_usd', 'volume_usd_24h', file_get_contents($apiUrl));
                file_put_contents($cacheFileName, $apiData);
                break;
            case 'xml':
                $apiData = $this->xmlToJson($apiUrl);
                file_put_contents($cacheFileName, $apiData);
                 break;
            default:
                file_put_contents($cacheFileName, file_get_contents($apiUrl));
        }
    }

    public function getData($cacheFileName, $apiUrl, $type='normal'){

        $curTime = time();
        $cacheFileName = __DIR__ . '/../../public/cache/' . $cacheFileName;

        if (!file_exists($cacheFileName)) {
        $this->writeCache($cacheFileName, $apiUrl, $type);
        } else {
            $fMtime = filemtime($cacheFileName);
            if (($curTime - $fMtime) > $this->expires) {
                $this->writeCache($cacheFileName, $apiUrl, $type);
            }
        }

        return file_get_contents($cacheFileName);

    }

    private function xmlToJson($apiUrl){
        $xml = file_get_contents($apiUrl);
        $data = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);

        $items = [];
        $i = 0;
        foreach ($data->channel->item as $item){
            $arrItem = [];
            $arrItem["title"] = (string) $item->title;
            $arrItem["description"] = strip_tags($item->description);
            $arrItem["fulltext"] = (string) $item->fulltext;
            $arrItem["date"] = (string) $item->pubDate;
            $img = (array) $item->enclosure;
            $arrItem["img"] = $img["@attributes"]["url"];
            $arrItem["id"] = $i;
            $items[] = $arrItem;
            $i++;
        }

        return json_encode($items);
    }


}